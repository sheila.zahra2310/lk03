/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas kalkulator, pertambahan, dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa abstract class akan lebih cocok digunakan daripada interface?
 * "Jawab disini..."
 * abstract class akan lebih cocok digunakan daripada interface ketika
 * child class dari kalkultor memiliki perilaku (method) yang sama dengan parent classnya, yaitu class kalkulator
 * abstract class ini memaksa semua child classnya agar memiliki ciri-ciri (atribut) dan perilaku (method) yang sama persis dengan parent classnya
 * child class juga hanya bisa di extend pada satu class abstract saja
 * namun abstract class ini boleh juga ditambahkan variable lain di dalam child classnya
 * jadi jika dalam abstract class kalkulator ini kedua child class memiliki method dengan sifat umum yg sama
 * dan tidak memiliki fungsi khusus yg berbeda, maka abstract class cocok digunakan 
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operand1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operand2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        kalkulator.setOperan(operand1, operand2);
        double result = kalkulator.hitung();
        System.out.println("Hasil: " + result);
    }
}