/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas pertambahan dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa interface akan lebih cocok digunakan daripada abstract class?
 * "Jawab disini..." 
 * interface akan lebih cocok digunakan daripada abstract class ketika
 * tiap child class dari kalkultor memiliki perilaku (method) khusus
 * child class juga bisa di implements pada lebih dari satu interface
 * sehingga dapat memiliki perilaku yang berbeda-beda
 * namun interface ini tidak boleh memiliki method selain yg terdapat pada parent-parent class yg di implemment
 * jadi jika dalam interface kalkulator ini kedua child class memiliki method dengan fungsi khusus yg berbeda,
 * maka interface lebih cocok digunakan 
 */

import java.util.*;

public class Main {   
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operan1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operan2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        double result = kalkulator.hitung(operan1, operan2);
        System.out.println("Result: " + result);
    }
}